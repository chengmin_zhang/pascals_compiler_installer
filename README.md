# Pascal-S 编译器安装说明

## 一、环境要求
- linux 64

- 建议为：Ubuntu 16.04 64位

- 程序所需的llvm和clang工具链会通过脚本自动安装

## 二、脚本说明
1. 脚本会自动检查当前文件夹下是否有可执行文件
2. 脚本会自动检查 g++ llvm 工具链是否存在于 /usr/bin/ 文件夹内
3. 如果不存在 g++ llvm clang 工具链，脚本会自动执行安装
4. 最后脚本会将 pascal 可执行文件复制进 usr/bin/ 内
5. 安装后会可以直接使用 pascal -help 查看使用方法

## 三、安装流程
1. 获取应用安装程序文件
```
git clone https://gitlab.com/chengmin_zhang/pascals_compiler_installer.git
```

2. 进入 pascals_compiler_installer 目录
```
cd pascals_compiler_installer
```

3. 运行installer.sh脚本
- 使脚本具有执行权限
```
chmod +x ./installer.sh
```

- 执行脚本文件
```
./installer.sh
```

## 四、使用说明
1. 使用pascal或pascal -help命令显示帮助
2. 使用 pascal [options] filename 进行编译
```
Options:
-T              Output AST Construct to stdout
-I              Output IR Code to stdout
-O              Output OPT IR Code to stdout
-S              Output Assembly Code to stdout
-A              Output ALL to stdout
```