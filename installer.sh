#!/bin/sh

pascals_execute_file="pascal"
compiler_support_path="/usr/bin/g++"
llvm_support_path="/usr/bin/llvm-as"
usr_bin_path="/usr/bin"
install_excutable_path="/usr/bin/pascal"

echo "-------- welcome to pascal-s comipler installer -------"

echo "step 1: checking the installer needed file"
if [ ! -x ${pascals_execute_file} ];
    then 
        echo "error : installer file damage! please re-download!"
        return
    else 
        echo "step 1: the excutable file existed"
        echo "step 1: finished checking installer needed file"
fi

echo "step 2: checking g++ support"
if [ ! -x ${compiler_support_path} ];
    then 
        echo "step 2: g++ support doesn't exist!"
        echo "step 2: start installing g++"
        sudo apt-get install g++
        echo "step 3: finished installing g++"
    else 
        echo "step 2: the g++ support existed"
        echo "step 2: finished checking g++ support"
fi

echo "step 3: checking llvm support"
if [ ! -x ${llvm_support_path} ];
    then 
        echo "step 3: llvm support doesn't exist!"
        echo "step 3: start installing llvm and clang"
        sudo apt-get install llvm clang
        echo "step 3: finished installing llvm and clang"
    else 
        echo "step 3: the llvm support existed"
        echo "step 3: finished checking llvm support"
fi

echo "step 4: copy excutable file"
if [ -x ${install_excutable_path} ];
    then 
        echo "step 4: delete the old excutable file"
        sudo rm ${install_excutable_path}
fi
echo "step 4: copy excutable file to /usr/bin/"
sudo cp ${pascals_execute_file} ${usr_bin_path}

echo "finish: you can use pascal -help see how to use"

echo "--------- pascal-s comipler installing finish ---------"